arqinp=data_cod.dat
pchk-file=pchk.dat
gen-file=gen.dat
arqout=data_decod.dat

<QAM>
#LEVELS
5
0.0
0.25
0.5
0.75
1.0

<MAP>
#NBITS
4
0000,0001,0010,0011,0100,0101,0111
1000,1001
1010,1011
1100,1101
1110,1111

