/*
 * calc_exit_bits_checks.cpp
 *
 *  Created on: 28 de mar de 2019
 *      Author: belli
 */


#include "calc_exit_bits_checks.h"
#include "genFuncs.h"
#include "calcexit.h"
#include <iostream>
#include <algorithm>
#include <numeric>
#include <map>
#include <cmath>

#define DEBUG_T(x)

double calc_exit_bits_to_dem(double j_check_to_bits, std::map<int, double> &pesos, int nSimuls, RandDataGsl &rand)
{
	double sig_l = calc_invJ(j_check_to_bits);
	double mi_l = sig_l * sig_l / 2.0;

	double sum_pesos = std::accumulate(pesos.begin(),pesos.end(), static_cast<double>(0.0), [](double v, std::map<int,double>::value_type &ptr)
			{
				return ptr.second + v;
			});

	//int nPesos = pesos.size();
	std::map<int,int> quantSimul;


	for(auto &val : pesos)
	{
		quantSimul[val.first] = static_cast<int>(val.second * static_cast<double>(nSimuls) / sum_pesos);
	}
	std::vector<RespCalcInfMutua_st> l_res;
	nSimuls = std::accumulate(quantSimul.begin(),quantSimul.end(), 0, [](int v, std::map<int,int>::value_type &ptr)
			{
				return ptr.second + v;
			});
	l_res.resize(nSimuls);
	int qualSimul = 0;


	for(auto ptr = quantSimul.begin(); ptr != quantSimul.end();++ptr)
	{
		for(int k = 0; k<ptr->second; ++k)
		{
			int i = ptr->first;
			bool bit = rand.getRndUnsInt() & 0x1;
			std::vector<double> lmsgs;
			lmsgs.resize(i+1);
			double media = bit?(mi_l):(-mi_l);
			for(int j=0;j<i+1;++j)
			{
				lmsgs[j] = media + rand.getGaussian(sig_l);
			}
			l_res[qualSimul].bit = bit;
			l_res[qualSimul].res = std::accumulate(lmsgs.begin(), lmsgs.end(), static_cast<double>(0.0));
			++qualSimul;
		}
	}

	double res = calc_infMutua(l_res);
	return res;
}


double calc_exit_bits_to_check(double j_check_to_bits, double j_dem_to_bits, std::map<int, double> &pesos, int nSimuls, RandDataGsl &rand)
{
	double sig_l = calc_invJ(j_check_to_bits);
	double mi_l = sig_l * sig_l / 2.0;
	double sig_l_db = calc_invJ(j_dem_to_bits);
	double mi_l_db = sig_l_db * sig_l_db / 2.0;

	double sum_pesos = std::accumulate(pesos.begin(),pesos.end(), static_cast<double>(0.0), [](double v, std::map<int,double>::value_type &ptr)
			{
				return ptr.second + v;
			});

	//int nPesos = pesos.size();
	std::map<int,int> quantSimul;


	for(auto &val : pesos)
	{
		quantSimul[val.first] = static_cast<int>(val.second * static_cast<double>(nSimuls) / sum_pesos);
	}
	std::vector<RespCalcInfMutua_st> l_res;
	nSimuls = std::accumulate(quantSimul.begin(),quantSimul.end(), 0, [](int v, std::map<int,int>::value_type &ptr)
			{
				return ptr.second + v;
			});
	l_res.resize(nSimuls);
	int qualSimul = 0;
	for(auto ptr = quantSimul.begin(); ptr != quantSimul.end();++ptr)
	{
		int i = ptr->first;
		for(int k = 0; k<ptr->second; ++k)
		{

			bool bit = rand.getRndUnsInt() & 0x1;

			std::vector<double> lmsgs;
			lmsgs.resize(i+1);
			double media = bit?(mi_l):(-mi_l);
			for(int j=0;j<i;++j)
			{
				lmsgs[j] = media + rand.getGaussian(sig_l);
			}
			double media2 = bit?(mi_l_db):(-mi_l_db);
			lmsgs[i] = media2 + rand.getGaussian(sig_l_db);
			l_res[qualSimul].bit = bit;
			l_res[qualSimul].res = std::accumulate(lmsgs.begin(), lmsgs.end(), static_cast<double>(0.0));
			++qualSimul;
		}
	}

	double res = calc_infMutua(l_res);
	return res;
}

double calc_exit_check_to_bits(double j_bits_to_checks, std::map<int, double> &pesos, int nSimuls, RandDataGsl &rand)
{
	double sig_l = calc_invJ(j_bits_to_checks);
	double mi_l = sig_l * sig_l / 2.0;

	double sum_pesos = std::accumulate(pesos.begin(),pesos.end(), static_cast<double>(0.0), [](double v, std::map<int,double>::value_type &ptr)
			{
				return ptr.second + v;
			});

	//int nPesos = pesos.size();
	std::map<int,int> quantSimul;


	for(auto &val : pesos)
	{
		quantSimul[val.first] = static_cast<int>(val.second * static_cast<double>(nSimuls) / sum_pesos);
		//std::cout << val.first << " = " << quantSimul[val.first] << " --";
	}
	//std::cout << std::endl;
	std::vector<RespCalcInfMutua_st> l_res;
	nSimuls = std::accumulate(quantSimul.begin(),quantSimul.end(), 0, [](int v, std::map<int,int>::value_type &ptr)
			{
				return ptr.second + v;
			});
	l_res.resize(nSimuls);
	int qualSimul = 0;


	for(auto ptr = quantSimul.begin();ptr != quantSimul.end();++ptr)
	{
		int i = ptr->first;
		//std::cout << "i=" << i << std::endl;
		for(int k = 0; k<ptr->second; ++k)
		{


			std::vector<double> lmsgs;
			lmsgs.resize(i-1);
			bool bitFinal = true;
			for(int j=0;j<(i-1);++j)
			{
				bool bit = rand.getRndUnsInt() & 0x1;

				double media = bit?(-mi_l):(+mi_l);
				lmsgs[j] = media + rand.getGaussian(sig_l);
				bitFinal ^=bit;
				//std::cout << " - b " << (bit?"1":"0") << "-v=" << lmsgs[j];
			}
			//std::cout << " - bitfinal=" << (bitFinal?"1":"0") << std::endl;
			double vv = 1.0;
			for(const auto &val : lmsgs)
			{
				double tt = tanh(val / 2.0);

				vv *= tt;
				//std::cout << " - val=" << val << ":tt="<<tt<<":vv=" << vv;
			}
			//std::cout<<std::endl;
			if(vv < -0.99999999)
			{
				l_res[qualSimul].res = -20;
			}
			else if(vv > 0.99999999)
			{
				l_res[qualSimul].res = 20;
			}
			else
			{
				l_res[qualSimul].res = 2.0 * atanh(vv);
			}
			//std::cout << "Sim res vv=" << vv << " - l_r=" << l_res[qualSimul].res << std::endl;
			l_res[qualSimul].bit = bitFinal;
			++qualSimul;
		}
	}
	double res = calc_infMutua(l_res);
	return res;

}




void calc_curv_exit_dem_to_bit_and_bit_to_dem(DataCalcExit_st *data, DataCalcAnswer_st *answer)
{
	double incremento_;
	std::vector<double> j_bits_to_dem;
	std::vector<double> j_check_to_bits;
	std::vector<double> j_dem_to_bits;
	std::vector<double> j_bits_to_checks;
	std::vector<double> j_bits_to_dem_complete;
	incremento_ = data->incremento;
	int nSims = floor( (0.999 - 0) / incremento_ ) + 1;
	j_bits_to_dem.resize( nSims);
	j_check_to_bits.resize( nSims);
	j_dem_to_bits.resize(nSims);
	j_bits_to_checks.resize(nSims);
	j_bits_to_dem_complete.resize(nSims);

	RandDataGsl rand;


	DEBUG_T(std::cout << "###################################################################################" << std::endl);
	DEBUG_T(std::cout << "            INICIANDO CALCULO BITS_TO_DEM - fbits1 - em função de CHECK_TO_BITS" << std::endl);
	for(int i=0;i<nSims;++i)
	{
		double j = incremento_ * static_cast<double>(i);
		j_bits_to_dem[i] = calc_exit_bits_to_dem(j, data->pesosBit, data->nSimuls, rand);
		DEBUG_T(std::cout << "j=" << j << " - res = " << j_bits_to_dem[i] << std::endl);
	}



	DEBUG_T(std::cout << "###################################################################################" << std::endl);
	DEBUG_T(std::cout << "            INICIANDO CALCULO CHECKS_TO_BITS - fcheck - em função de BITS_TO_CHECK" << std::endl);
	for(int i=0;i<nSims;++i)
	{
		double j = incremento_ * static_cast<double>(i);
		j_check_to_bits[i] = calc_exit_check_to_bits(j, data->pesosCheck, data->nSimuls, rand);
		DEBUG_T(std::cout << "j=" << j << " - res = " << j_check_to_bits[i] << std::endl);
	}


	DEBUG_T(std::cout << "###################################################################################" << std::endl);
	DEBUG_T(std::cout << "            INICIANDO CALCULO DEM_TO_BITS - fdemmod - em função de BITS_TO_DEM" << std::endl);
	DEBUG_T(std::cout << "sigma=" << data->sigma << std::endl);
	for(int i=0;i<nSims;++i)
	{
		double j = incremento_ * static_cast<double>(i);
		j_dem_to_bits[i] = calcExitDecInc2(data->mapInv2, data->nBits, data->levels, j, data->sigma, data->nSimuls, rand);
		DEBUG_T(std::cout << "j=" << j << " - res = " << j_dem_to_bits[i] << std::endl);
	}

	DEBUG_T(std::cout << "###################################################################################" << std::endl);
	DEBUG_T(std::cout << "            INICIANDO CALCULO BITS_TO_CHECK em função de CHECK_TO_BITS" << std::endl);
	for(int i=0;i<nSims;++i)
	{
		double j_db = incremento_ * static_cast<double>(i);
		double j_bd = calc_inv_func(j_dem_to_bits, incremento_, j_db);
		double j_cb = calc_inv_func(j_bits_to_dem, incremento_, j_bd);
		//std::cout << "jdb=" << j_db << " - jbd=" << j_bd << " - j_cd=" << j_cb << std::endl;
		j_bits_to_checks[i] = calc_exit_bits_to_check(j_cb, j_db, data->pesosBit, data->nSimuls, rand);
		DEBUG_T(std::cout << "j=" << j_db << " - res=" << j_bits_to_checks[i] << std::endl);
	}

	DEBUG_T(std::cout << "###################################################################################" << std::endl);
	DEBUG_T(std::cout << "            INICIANDO CALCULO BITS_TO_DEM em função de DEM_TO_BITS - considerando todo o LDPC" << std::endl);
	for(int i=0;i<nSims;++i)
	{
		double j_db = incremento_ * static_cast<double>(i);
		double j_bc = j_bits_to_checks[i];
		double j_cb = calc_exit_check_to_bits(j_bc, data->pesosCheck, data->nSimuls, rand);
		j_bits_to_dem_complete[i] = calc_exit_bits_to_dem(j_cb, data->pesosBit, data->nSimuls, rand);
		DEBUG_T(std::cout << "j_db=" << j_db << " - j_bc=" << j_bc << " - j_cb = " << j_cb << " - j_bits_to_dem_complete=" << j_bits_to_dem_complete[i] << std::endl);
	}

	answer->j_bits_to_dem.resize(nSims);
	answer->j_dem_to_bits.resize(nSims);

	for(int i=0;i<nSims;++i)
	{
		answer->j_bits_to_dem[i] = j_bits_to_dem_complete[i];
		answer->j_dem_to_bits[i] = j_dem_to_bits[i];
	}

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class FillQuantCl
{
public:
	struct ValueInt_st
	{
		int n;
		double pos;
	};
	std::vector<ValueInt_st> values;
	int maxIndex;
	RandDataGsl &rand;
	FillQuantCl(const std::map<int, double> &pesos, RandDataGsl &rand) : rand(rand)
	{
		maxIndex = 0;
		double pesoTotal = 0;
		pesoTotal = std::accumulate(pesos.begin(), pesos.end(), 0.0, [](double v, const std::pair<const int,double> &d) mutable
				{
					return v+d.second;
				});

		maxIndex = (--pesos.end())->first;

		values.resize(pesos.size());
		double incAt = 0.0;
		int i = 0;
		for(auto &v : pesos)
		{
			incAt += v.second / pesoTotal;
			ValueInt_st vv;
			vv.n = v.first;

			vv.pos = incAt;
			values[i++] = vv;
		}
	}

	int getMaxIndex()
	{
		return maxIndex;
	}

	void fillQuantityPerBit(std::vector<int> *quant)
	{

		for(int i=0;i<quant->size();++i)
		{
			double v = rand.getUniform();
			int n = -1;
			for(auto &val : values)
			{
				if(v < val.pos)
				{
					n = val.n;
				}
			}
			if(n > 0)
			{
				(*quant)[i] = n;
			}
			else
			{
				(*quant)[i] = values[values.size()-1].n;
			}
		}
	}

};

struct MessageCheckToBits_st
{
	double msg;
	bool bit;
};

static void initializeValuesCheckToBits(std::vector<int> &quant, std::vector<std::vector<MessageCheckToBits_st>> *m_check_to_bits)
{
	int size1 = quant.size();
	for(int i=0;i<size1;++i)
	{
		for(int k=0;k<quant[i];++k)
		{

		}
	}
}

double calc_exit_bits_to_check_from_check_to_bits(double j_bits_to_checks, std::map<int, double> &pesos, int nSimuls, int nBitsPerSimb, RandDataGsl &rand)
{
	std::vector<int> check_to_bit_size;
	check_to_bit_size.resize(nBitsPerSimb);

	FillQuantCl quant(pesos, rand);



	std::vector<std::vector<MessageCheckToBits_st>> m_check_to_bits;
	m_check_to_bits.resize(nBitsPerSimb);
	for(auto &val : m_check_to_bits)
	{
		val.resize(quant.getMaxIndex());
	}

	for(int sim=0;sim<nSimuls;++sim)
	{
		quant.fillQuantityPerBit(&check_to_bit_size);

		initializeValuesCheckToBits(check_to_bit_size, &m_check_to_bits);
	}

	return 0.0;
}


void calc_curv_exit_bits_to_check_and_check_to_bits(DataCalcExit_st *data, DataCalcAnswer2_st *answer)
{
	double incremento_;
	std::vector<double> j_check_to_bits;
	std::vector<double> j_bits_to_check;
	incremento_ = data->incremento;
	int nSims = floor( (0.999 - 0) / incremento_ ) + 1;
	j_check_to_bits.resize(nSims);
	j_bits_to_check.resize(nSims);

	RandDataGsl rand;

	DEBUG_T(std::cout << "###################################################################################" << std::endl);
	DEBUG_T(std::cout << "            INICIANDO CALCULO CHECK_TO_BITS -  - em função de BITS_TO_CHECK" << std::endl);
	for(int i=0;i<nSims;++i)
	{
		double j = incremento_ * static_cast<double>(i);
		j_check_to_bits[i] = calc_exit_check_to_bits(j, data->pesosCheck, data->nSimuls, rand);
		DEBUG_T(std::cout << "j=" << j << " - res = " << j_check_to_bits[i] << std::endl);
	}

	DEBUG_T(std::cout << "###################################################################################" << std::endl);
	DEBUG_T(std::cout << "            INICIANDO CALCULO BITS_TO_CHECK -  - em função de CHECK_TO_BIT" << std::endl);

	for(int i=0;i<nSims;++i)
	{
		double j = incremento_ * static_cast<double>(i);
		j_bits_to_check[i] = calc_exit_bits_to_check_from_check_to_bits(j, data->pesosCheck, data->nSimuls, data->nBits, rand);
		DEBUG_T(std::cout << "j=" << j << " - res = " << j_check_to_bits[i] << std::endl);
	}


}





