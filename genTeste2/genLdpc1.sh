#!/bin/bash

localdir=`pwd`

echo "Iniciando"

export LD_LIBRARY_PATH=${localdir}/..:${localdir}/../src_orig/lib/:${LD_LIBRARY_PATH}

echo "${LD_LIBRARY_PATH}"

../make-ldpc pchkm_2200_3000_3.dat 2200 3000 1 evencol 3 no4cycle
../make-gen  pchkm_2200_3000_3.dat gen_2200_3000_3.dat sparse

../geninputfile inpfile.dat 100 3
