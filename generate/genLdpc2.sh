#!/bin/ksh
#
# @ class = serial
# @ restart = no
# @ output = saida_genldpc2.out
# @ error =  saida_genldpc2.err
# @ queue

localdir=`pwd`

export LD_LIBRARY_PATH="${localdir}/..";${LD_LIBRARY_PATH}

echo ${LD_LIBRARY_PATTH}


../make-ldpc pchkm_50000_70000_mm8_mm10.dat 50000 70000 1 evencol 6x3/1.5x2/0.1x8/0.05x10 no4cycle
../make-gen  pchkm_50000_70000_mm8_mm10.dat gen_50000_70000_mm8_mm10.dat sparse

../make-ldpc pchkm_50000_70000_mm8_ss10.dat 50000 70000 1 evencol 6x3/1.5x2/0.1x8 no4cycle
../make-gen  pchkm_50000_70000_mm8_ss10.dat gen_50000_70000_mm8_ss10.dat sparse
