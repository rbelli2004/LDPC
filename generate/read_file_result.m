function [ruido, erros] =  read_file_result(filename)

  comp_inicio='Result:';

  fid = fopen(filename, 'r');

  erros=[];
  
  ruido = [];

  while(1)
    line=fgets(fid);
    if(line < 0)
      break;
    endif
    
    if(!strcmp(line(1:length(comp_inicio)), comp_inicio))
      #disp(line)
      continue;
    #else
      #disp('achado na linha');
      #disp(line)
    endif
    
    [val, count,errmsg] = sscanf(line, 'Result: Noise=%f Bits: %d - errors=%d Taxa=%f - taxaSemFec=%f');
    if(count < 4)
       disp(errmsg);
    endif
    
    
    erros = [erros val(3)/val(2)];
    ruido=[ruido val(1)];
    
  endwhile

endfunction

