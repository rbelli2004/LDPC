#!/bin/ksh
#
# @ class = serial
# @ restart = no
# @ output = saida_genbig.out
# @ error =  saida_genbig.err
# @ queue


localdir=`pwd`

echo "Iniciando"

export LD_LIBRARY_PATH=${localdir}/../..:${localdir}/../../src_orig/lib/:${LD_LIBRARY_PATH}

echo "${LD_LIBRARY_PATH}"


# ../make_ldpc  M N 
#K = N - M bits -> K bits na entrada produzem N bits na saida, com M bits de paridade

../../make-ldpc pchkm_4000_8000_6.dat 4000 8000 1 evenboth 6 no4cycle
../../make-gen pchkm_4000_8000_6.dat gen_4000_8000_6.dat  sparse

../../make-ldpc pchkm_4000_8000_4.dat 4000 8000 1 evenboth 4 no4cycle
../../make-gen pchkm_4000_8000_4.dat gen_4000_8000_4.dat  sparse

../../make-ldpc pchkm_4000_8000_8.dat 4000 8000 1 evenboth 8 no4cycle
../../make-gen pchkm_4000_8000_8.dat gen_4000_8000_8.dat  sparse

#for i in $tamanhos; do
#    k=`expr $i - 1000`
#    f='4'
#    echo "val ${k} ${i} ${f}"
#    echo "val ${k} ${i} ${f}" >> teste.txt
#    ../../make-ldpc pchkm_${k}_${i}_1.dat ${k} ${i} 1 evenboth ${f} no4cycle
#    ../../make-gen  pchkm_${k}_${i}_1.dat gen_${k}_${i}_1.dat sparse
#done


