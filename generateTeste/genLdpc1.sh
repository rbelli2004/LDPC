#!/bin/bash

localdir=`pwd`

echo "Iniciando"

export LD_LIBRARY_PATH=${localdir}/..:${localdir}/../src_orig/lib/:${LD_LIBRARY_PATH}

echo "${LD_LIBRARY_PATH}"

../make-ldpc pchkm_1000_2000.dat 1000 2000 1 evencol 6 no4cycle
../make-gen  pchkm_1000_2000.dat gen_1000_2000.dat sparse

../geninputfile inpfile.dat 125 0
