#!/bin/bash

localdir=`pwd`

echo "Iniciando"

export LD_LIBRARY_PATH=${localdir}/..:${localdir}/../src_orig/lib/:${LD_LIBRARY_PATH}

echo "${LD_LIBRARY_PATH}"

tamanhos="1500 2000 2500 3000 3500 4000"

# ../make_ldpc  M N 
#K = N - M bits -> K bits na entrada produzem N bits na saida, com M bits de paridade

for i in $tamanhos; do
    k=`expr $i - 1000`
    f=`expr 4 + $k / 500`
    echo "val ${k} ${i} ${f}"
    echo "val ${k} ${i} ${f}" >> teste.txt
    ../make-ldpc pchkm_${k}_${i}.dat ${k} ${i} 1 evencol ${f} no4cycle
    ../make-gen  pchkm_${k}_${i}.dat gen_${k}_${i}.dat sparse
done


