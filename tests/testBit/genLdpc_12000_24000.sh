#!/bin/ksh
#
# @ class = serial
# @ restart = no
# @ output = saida_genbig3_3.out
# @ error =  saida_genbig3_3.err
# @ queue


localdir=`pwd`

echo "Iniciando"

export LD_LIBRARY_PATH=${localdir}/../..:${localdir}/../../src_orig/lib/:${LD_LIBRARY_PATH}

echo "${LD_LIBRARY_PATH}"


# ../make_ldpc  M N 
#K = N - M bits -> K bits na entrada produzem N bits na saida, com M bits de paridade



../../make-ldpc pchkm_12000_24000_3.dat 12000 24000 1 evenboth 3 no4cycle
../../make-gen pchkm_12000_24000_3.dat gen_12000_24000_3.dat  sparse

../../make-ldpc pchkm_12000_24000_3_2.dat 12000 24000 1 evenboth 5x3/2x2 no4cycle
../../make-gen pchkm_12000_24000_3_2.dat gen_12000_24000_3_2.dat  sparse

../../make-ldpc pchkm_12000_24000_3_2_e.dat 12000 24000 1 evenboth 2x3/2x2 no4cycle
../../make-gen pchkm_12000_24000_3_2_e.dat gen_12000_24000_3_2_e.dat  sparse


#for i in $tamanhos; do
#    k=`expr $i - 1000`
#    f='4'
#    echo "val ${k} ${i} ${f}"
#    echo "val ${k} ${i} ${f}" >> teste.txt
#    ../../make-ldpc pchkm_${k}_${i}_1.dat ${k} ${i} 1 evenboth ${f} no4cycle
#    ../../make-gen  pchkm_${k}_${i}_1.dat gen_${k}_${i}_1.dat sparse
#done


