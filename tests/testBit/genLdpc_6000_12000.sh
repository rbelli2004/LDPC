#!/bin/ksh
#
# @ class = serial
# @ restart = no
# @ output = saida_genbig2.out
# @ error =  saida_genbig2.err
# @ queue


localdir=`pwd`

echo "Iniciando"

export LD_LIBRARY_PATH=${localdir}/../..:${localdir}/../../src_orig/lib/:${LD_LIBRARY_PATH}

echo "${LD_LIBRARY_PATH}"


# ../make_ldpc  M N 
#K = N - M bits -> K bits na entrada produzem N bits na saida, com M bits de paridade


../../make-ldpc pchkm_6000_12000_4.dat 6000 12000 1 evenboth 4 no4cycle
../../make-gen pchkm_6000_12000_4.dat gen_6000_12000_4.dat  sparse

../../make-ldpc pchkm_6000_12000_3.dat 6000 12000 1 evenboth 3 no4cycle
../../make-gen pchkm_6000_12000_3.dat gen_6000_12000_3.dat  sparse

#for i in $tamanhos; do
#    k=`expr $i - 1000`
#    f='4'
#    echo "val ${k} ${i} ${f}"
#    echo "val ${k} ${i} ${f}" >> teste.txt
#    ../../make-ldpc pchkm_${k}_${i}_1.dat ${k} ${i} 1 evenboth ${f} no4cycle
#    ../../make-gen  pchkm_${k}_${i}_1.dat gen_${k}_${i}_1.dat sparse
#done


