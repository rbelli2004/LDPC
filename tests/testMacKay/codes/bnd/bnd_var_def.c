  bndc->loops = 20 ;          /* -bndloops loops    */
  bndc->loop = 0 ;            /* - -                */
  bndc->writelog = 0 ;        /* -bndlogn n         */
  bndc->doclip = 1 ;          /* -doclip doclip     */
  bndc->clip = 0.9999999999 ; /* -clip clip         */
  bndc->tinydiv = 1e-40 ;     /* -tinydiv tiny      */
  bndc->dofudge = 0 ;         /* -dofudge dofudge   */
  bndc->fudge = 1.0 ;         /* -fudge fudge       */
