  DNT; fprintf( fp, "-v verbose <%d>                (verbosity           )", c->verbose);
  DNT; fprintf( fp, "-Ain Afile                    (file: alist 1       )");
  DNT; fprintf( fp, "-Aout Afile                   (file: alist 2       )");
  DNT; fprintf( fp, "-G Gfile                      (file: G             )");
  DNT; fprintf( fp, "-Cn Cnfile                    (file: Cn (alist format))");
  DNT; fprintf( fp, "-force force <%d>              (whether to forcibly invert Cn (and how many extra 1s per col and row to allow in A))", c->force);
