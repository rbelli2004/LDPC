  int verbose ;     /* verbosity            <0>    */
  char rfile[100] ; /* file: r vector       <->    */
  int N ;           /* number of bits in block <->    */
  char Cnfile[100] ;/* file: Cn matrix in alist format <->    */
  int rsuffix ;     /* rfile: add suffices .0001,.0002... <0>    */
  int zsuffix ;     /* whether zfile should have suffices .0001,.0002... appended <0>    */
  char zfile[100] ; /* z vector             <->    */
