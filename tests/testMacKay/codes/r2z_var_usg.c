  DNT; fprintf( fp, "-v verbose <%d>                (verbosity           )", c->verbose);
  DNT; fprintf( fp, "-rfile rfile                  (file: r vector      )");
  DNT; fprintf( fp, "-n n                          (number of bits in block)");
  DNT; fprintf( fp, "-Cnfile Cn                    (file: Cn matrix in alist format)");
  DNT; fprintf( fp, "-rsuffix rs <%d>               (rfile: add suffices .0001,.0002...)", c->rsuffix);
  DNT; fprintf( fp, "-zsuffix zs <%d>               (whether zfile should have suffices .0001,.0002... appended)", c->zsuffix);
  DNT; fprintf( fp, "-zfile zfile                  (z vector            )");
