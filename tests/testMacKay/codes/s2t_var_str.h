  int verbose ;     /* verbosity            <0>    */
  char sfile[100] ; /* file: s vector       <->    */
  int K ;           /* number of source bits <->    */
  int N ;           /* number of noise bits <->    */
  char Gfile[100] ; /* file: G matrix       <->    */
  int tsuffix ;     /* whether tfile should have suffices .0001,.0002... appended <0>    */
  char tfile[100] ; /* t vector             <->    */
  int smn ;         /* whether SYSTEMATIC MN code <0>    */
