  DNT; fprintf( fp, "-v verbose <%d>                (verbosity           )", c->verbose);
  DNT; fprintf( fp, "-sfile sfile                  (file: s vector      )");
  DNT; fprintf( fp, "-k k                          (number of source bits)");
  DNT; fprintf( fp, "-n n                          (number of noise bits)");
  DNT; fprintf( fp, "-Gfile Gfile                  (file: G matrix      )");
  DNT; fprintf( fp, "-tsuffix ts <%d>               (whether tfile should have suffices .0001,.0002... appended)", c->tsuffix);
  DNT; fprintf( fp, "-tfile tfile                  (t vector            )");
  DNT; fprintf( fp, "-smn smn <%d>                  (whether SYSTEMATIC MN code)", c->smn);
