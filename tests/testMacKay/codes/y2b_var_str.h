  int verbose ;     /* verbosity            <0>    */
  char yfile[100] ; /* file: y vector       <->    */
  int N ;           /* block length         <->    */
  int bsuffix ;     /* whether bfile should have suffices .0001,.0002... appended <0>    */
  int ysuffix ;     /* whether yfile should have suffices .0001,.0002... appended <0>    */
  char bfile[100] ; /* b vector             <->    */
  double gcx ;      /* signal_to_noise ratio <1.0>  */
