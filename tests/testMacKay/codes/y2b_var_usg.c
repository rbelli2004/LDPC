  DNT; fprintf( fp, "-v verbose <%d>                (verbosity           )", c->verbose);
  DNT; fprintf( fp, "-yfile yfile                  (file: y vector      )");
  DNT; fprintf( fp, "-n n                          (block length        )");
  DNT; fprintf( fp, "-bsuffix bs <%d>               (whether bfile should have suffices .0001,.0002... appended)", c->bsuffix);
  DNT; fprintf( fp, "-ysuffix ys <%d>               (whether yfile should have suffices .0001,.0002... appended)", c->ysuffix);
  DNT; fprintf( fp, "-bfile bfile                  (b vector            )");
  DNT; fprintf( fp, "-gcx gcx <%9.3g>       (signal_to_noise ratio)", c->gcx);
